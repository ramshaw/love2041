Basic dating website implemented in Python / CGI.

Features

* Matches user with similar people.

* View and edit profile.

* Photo viewer, upload, delete photos.

* Create new user account (sort of working)

* Password authentication

* Other stuff