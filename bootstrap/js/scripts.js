function login(username)
{
    document.cookie="loggedIn=1";
    document.cookie="user="+username;
    localStorage.setItem("user", username);
    location.reload();
}

function loginNewUser(username)
{
    document.cookie="loggedIn=1";
    document.cookie="user="+username;
    localStorage.setItem("user", username);
}

function logout()
{
    var cookies = $.cookie();
    for(var cookie in cookies) {
        $.removeCookie(cookie);
    }
    location.reload();
}

function rememberPage(num)
{
    document.cookie="currPage="+num;
}

function showUsername()
{
    var user = localStorage.getItem("user")
    document.getElementById("loginSig").innerHTML = "Signed in as " + user;
    function showAlert() { alert('You triggered an alert!'); }
}

$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
}); 

function elastForms()
{
    $('#dynamicForm').elastic();
}
