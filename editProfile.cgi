#!/usr/bin/python

import cgitb, cgi, re, sys, os, Cookie, html
from collections import defaultdict
import difflib

cgitb.enable()

# get the correct user, don't allow non-user to edit profile
form = cgi.FieldStorage()
try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    currUser = cookie['user'].value
except:
    currUser = "SexyDude88"

htmlBody = html.navbar(currUser, "edit")

# Get profile.txt data
f = open('students/%s/profile.txt' % (currUser))
profileText = ''
for line in f:
    profileText += line
f.close()

# check for delete photo and make dp flags
swapped = 0
for i in range(0,100):
    try:
        dp = form['delphoto%02d.jpg' % i]
        try:
            os.remove('students/%s/photo%02d.jpg' % (currUser, i))
            htmlBody += html.alert("Removing photo")
        except:
            htmlBody += html.alertFail("Failed to remove photo")
    except:
        htmlBody += ""
    if swapped == 0:
        try:
            swap = form['dpphoto%02d.jpg' % i]
            try:
                os.rename('''students/%s/profile.jpg''' % currUser, '''students/%s/profileOld.jpg''' % currUser)
                os.rename('''students/%s/photo%02d.jpg''' % (currUser, i), '''students/%s/profile.jpg''' % currUser )
                os.rename('''students/%s/profileOld.jpg''' % currUser, '''students/%s/photo%02d.jpg''' % (currUser, i) )
                htmlBody += html.alert("Change profile photo")
                swapped = 1
            except:
                htmlBody += html.alertFail("Failed to change profile photo")
        except:
            htmlBody += ""

# Make array of photo file names
photosList = os.listdir('students/'+currUser)
photos = list()
lastPhoto = ""

for i in photosList:
    if re.match(r'photo[0-9][0-9].[(jpg)(png)]', i):
        photos.append(i)
        lastPhoto = i

# Check for new upload photo
try:
    profilePic = form['file']
    try:
        if lastPhoto != '':
            lastPhoto = re.sub(r'photo', '', lastPhoto)
            lastPhoto = re.sub(r'\.jpg', '', lastPhoto)
            lastPhoto = int(lastPhoto)
            lastPhoto += 1
        else:
            lastPhoto = 0
        fn = '''students/%s/photo%02d.jpg''' % (currUser, lastPhoto)
        f = open(fn , 'wb')
        f.write(profilePic.file.read())
        f.close()
        htmlBody += html.alert("Photo upload success")
    except:
        htmlBody += html.alertFail("Photo upload failed")
except:
    htmlBody += ""


profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)
profileFields = defaultdict(str)
modified = 0

# test to see if fields have been updated, if so, write to file.
# check allFields, even those not specified in file
allFields = ['profile_text', 'birthdate', 'courses', 'degree', 'email', 'favourite_bands', 'favourite_books', 'favourite_hobbies', 'favourite_movies', 'gender', 'hair_colour', 'height', 'name', 'password', 'username', 'weight']

for x in range(0, len(profileText)):
    if re.match(r'^[a-z_]+:', profileText[x]):
        field = re.sub(r'( *:.*)|(\n*)', '', profileText[x])
        profileFields[field] = profileText[x+1]

existingKeys = profileFields.keys()
newKeys = form.keys()

for f in allFields:
    if f in newKeys:
        if form[f].value != "":
            new = form[f].value
            new = re.sub(r'<.*>[^<\>]*</.*>', '', new)
            newLines = re.split("\n", new)
            newOut = ''
            for n in newLines:
                newOut += "\t" + re.sub(r'\s$', '', n) + "\n"
            if not re.search(profileFields[f], newOut):
                htmlBody += html.alert("Updated " + re.sub(r'_', ' ', f).title())
            elif f not in existingKeys:
                htmlBody += html.alert("Added " + re.sub(r'_', ' ', f).title())
            profileFields[f] = newOut
            modified = 1

# Write updated file
if modified == 1:
    keys = profileFields.keys()
    try:
        newProfile = ''
        for k in keys:
            newProfile += k + ":\n" + profileFields[k]
        f = open('students/%s/profile.txt' % (currUser), "w")
        f.write(newProfile)
        f.close()
    except:
        htmlBody += html.alertFail("Could not write to file")

# generate html output
htmlBody += html.editProfile(profileFields, photos)

print html.html(htmlBody)