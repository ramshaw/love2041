#!/usr/bin/python

import cgitb, cgi, re, sys, os, Cookie, html, random
from collections import defaultdict

cgitb.enable()

def html(body):
	return '''Content-type: text/html; charset=utf-8\n
	<!DOCTYPE html>
    <html lang="en">
    <head>
    <title>Aaron's LOVE2041</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="bootstrap/lightbox-master/ekko-lightbox.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Arimo|Open+Sans|Oxygen' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="bootstrap/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-cookie.js"></script>
    <script type="text/javascript" src="bootstrap/js/scripts.js"></script>
    <script type="text/javascript" src="bootstrap/lightbox-master/ekko-lightbox.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery.elastic.source.js"></script>
    </head><body><div class="container-fluid">%s</div>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    </body></html>''' % body

def navbar( user, active ):
    html = '''
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="love2041.cgi"><font face="verdana" color="black"><span class="glyphicon glyphicon-heart"></span></font> LOVE2041</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left" method="post" role="search" action="searchResults.cgi">
                <div class="form-group">
                    <input type="text" class="form-control" name="searchQuery" placeholder="Search for user">
                </div>
                <button type="submit" class="btn btn-default">Go!</button>
            </form>
            <ul class="nav navbar-nav"> '''
    if re.match(active, "matches"):
        html += '''<li class="active"><a href="love2041.cgi">My Matches</a></li>'''
    else:
        html += '''<li><a href="love2041.cgi">My Matches</a></li>'''
    if re.match(active, "edit"):
        html += '''<li class="active"><a href="editProfile.cgi?user=%s">%s</a></li>''' % (user, user)
    else:
        html += '''<li><a href="editProfile.cgi?user=%s">%s</a></li>''' % (user, user)
    if re.match(active, "settings"):
        html += '''<li class="active"><a href="settings.cgi">Settings</a></li>'''
    else:
        html += '''<li><a href="settings.cgi">Settings</a></li>'''
    html += '''
                <li><a href="#" onclick="logout()">Logout</a></li>
            </ul>
             
        </div>

        </ul>
    </nav>
    '''
    return html

def loginPage():
    return '''<body>
    <h1>Welcome to LOVE2041</h1>
    <h2>Please log in</h2>
    <form action="love2041.cgi?page=1" method="post">
    <p>Username: <input type="text" name="username"></p>
    <p>Password: <input type="password" name="password"></p>
    <p><input type="submit" class="btn btn-default" value="Submit"/></p>
    </form>

    <h3>Don't have an account?</h3>
    <form action="register.cgi" method="post">
    <input type="submit" class="btn btn-default" value="Sign Up"/>
    </form>
    '''

def panel(title, contents):
    r = '''
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2 class="panel-title">%s</h2>
          </div>
          <div class="panel-body">
          %s
          </div>
        </div>
    </div>
    ''' % (title, contents)
    return r

def largeProfile(user, name, degree, birthday, email, gender):
    r = '''
    <div class="media">
        <a class="pull-left" href="viewProfile.cgi?user=%s">
            <img class="media-object" src=\"students/%s/profile.jpg?=%s" max-width="200"/>
        </a>
        <div class="media-body">
            <h3 class="media-heading">%s</h3>
            <p>Name: %s<br>
               Degree: %s<br>
               Birthday: %s<br>
               Email: %s<br>
               Gender: %s<br>
            </p>

            <form action="editProfile.cgi?user=%s" method="post">
                <input type="submit" class="btn btn-default" value="Edit" />
            </form>
        </div>
    </div>
    ''' % (user, user, random.randint(10000,100000), user, name, degree, birthday, email, gender, user)
    return r

def smallProfile(user, name, degree):
    r = '''
    <div class="media">
        <a class="pull-left" href="viewProfile.cgi?user=%s">
            <img class="media-object" src=\"students/%s/profile.jpg?%s\"/>
        </a>
        <div class="media-body">
            <h3 class="media-heading">
            <a href="viewProfile.cgi?user=%s">%s</h3></a>
            <p>Name: %s<br>
               Degree: %s<br>
            </p>
        </div>
    </div>
    ''' % (user, user, random.randint(10000,100000), user, user, name, degree)
    return r

def profileListEntry(selfUser, user, selfFields, fields):
    r = '''
    <div class="media">
        <a class="pull-left" href="viewProfile.cgi?user=%s">
            <img class="media-object" src=\"students/%s/profile.jpg?%s\"/>
        </a>
        <div class="media-body">
            <h3 class="media-heading">
            <a href="viewProfile.cgi?user=%s">%s</h3></a>

            <ul class="list-group">
                <li class="list-group-item"><b>Name:</b> %s</li>
                <li class="list-group-item"><b>Degree:</b> %s</li>
    ''' % (user, user, random.randint(10000,100000), user, user, fields['name'], fields['degree'])
    if fields.has_key("profile_text"):
        r += '''<li class="list-group-item"><b>Bio:</b> %s</li>''' % fields["profile_text"]

    sk = selfFields.keys()

    for key in sk:
        if fields.has_key(key):
            if re.search(selfFields[key], fields[key]) or re.search(fields[key], selfFields[key]):
                k = re.sub(r'_', ' ', key)
                r += '''<li class="list-group-item"><b>Has the same %s as you:</b> %s</li>''' % (k, fields[key])

    prefs = getPrefs(selfUser)
    sk = prefs.keys()
    for key in sk:
        if fields.has_key(key):
            if re.search(re.sub(r'\W', '', fields[key]), re.sub(r'\W','', prefs[key]) ) or re.search(re.sub(r'\W','', prefs[key]), re.sub(r'\W', '', fields[key])):
                k = re.sub(r'_', ' ', key)
                r += '''<li class="list-group-item"><b>Your preferred %s:</b> %s</li>''' % (k, fields[key])

    if fields.has_key("birthdate"):
        bd = int( birthyear(fields["birthdate"]) )
        bd1 = int( birthyear(selfFields["birthdate"]) )
        if bd1/2 + 7 >= bd:
            r += '''<li class="list-group-item"><b>Acceptable age</b></li>'''
        else:
            if bd > bd1:
                if bd-bd1 < 10:
                    r += '''<li class="list-group-item"><b>Around your age</b></li>'''
            if bd1 > bd:
                if bd1-bd < 10:
                    r += '''<li class="list-group-item"><b>Around your age</b></li>'''

    r += '''

            </ul>
        </div>
    </div> '''

    return r

def fullProfile(fields, photos):
    k = fields.keys()

    left = '''<div class="container-fluid">
                  <div class="row-eq-height">
                      <a class="col-lg-12 col-md-12 thumbnail" data-toggle="lightbox" href="students/%s/profile.jpg?%s">
                        <img class="img-thumb" src="students/%s/profile.jpg?%s" alt="">
                      </a>
                  </div>''' % ( fields['username'], random.randint(10000,100000), fields['username'], random.randint(10000,100000))

    left += '''<div class="row" id="equalBoxes">'''

    for p in photos:
        left += '''<div class="onesquare"><a class="col-lg-4 col-md-4 thumbnail" data-toggle="lightbox" href="students/%s/%s">
                        <img class="img-thumb" src="students/%s/%s" alt="">
                  </a></div>''' % (fields['username'], p, fields['username'], p)

    left += '''</div>
                </div>'''

    right = '''
    <div class="panel panel-default">
        <div class="panel-heading"><h3>%s</h3></div>
        <ul class="list-group"> ''' % (fields['username'])

    for field in k:
        if( not re.match(r'(password)|(email).*', field) ):
            f = field.title()
            f = re.sub(r'_', ' ', f)
            value = fields[field]
            value = re.sub(r'\s*$', '', value)
            valueList = value.split("\n")
            valueString = ""
            if len(valueList) > 1:
                for v in valueList:
                    valueString += '''<p>%s</p>''' % (v)
            else:
                valueString += valueList[0]
            right += '''        <li class="list-group-item"><b>%s:</b> %s</li> ''' % (f, valueString)
    right += '''
        </ul>
    </div>
    '''

    return split( right, panel("Photos", left ) );

def editProfile(fields, photos):
    k = fields.keys()

    left = '''<div class="container-fluid">
                  <div class="row-eq-height">
                      <a class="col-lg-12 col-md-12 thumbnail" data-toggle="lightbox" href="students/%s/profile.jpg?%s">
                        <img class="img-thumb" src="students/%s/profile.jpg?%s" alt="">
                      </a>
                  </div>''' % (fields['username'], random.randint(10000,100000), fields['username'], random.randint(10000,100000))

    left += '''<div class="row" id="equalBoxes">'''

    for p in photos:
        left += '''<div class="onesquare"><a class="col-lg-4 col-md-4 thumbnail">
                        <img class="img-thumb" src="students/%s/%s?%s" alt="">
                        <form method="post" action="editProfile.cgi">
                            <button type="submit" name="del%s" value="true" class="btn btn-default" id="fullTop">
                                <span class="glyphicon glyphicon-remove-circle"> Delete</span>
                            </button>
                            <button type="submit" name="dp%s" value="true" class="btn btn-default" id="fullBottom">
                                <span class="glyphicon glyphicon-pushpin"> Default</span>
                            </button>
                        </form>
                  </a></div>''' % (fields['username'], p, random.randint(10000,100000), p, p)

    left += "</div>"

    left += '''<form enctype="multipart/form-data" action="editProfile.cgi" method="post">
                    <p><h3>Upload photo:</h3><input type="file" name="file"></p>
                    <p><input type="submit" class="btn btn-default" value="Submit"/></p>
                </form>'''

    left += '''
                </div>'''



    right = '''
    <div class="panel panel-default">
        <div class="panel-heading"><h3>Editing profile %s</h3></div>
        <ul class="list-group"> ''' % (fields['username'])

    right += '''<form method="post">'''

    allFields = ['profile_text', 'birthdate', 'courses', 'degree', 'email', 'favourite_bands', 'favourite_books', 'favourite_hobbies', 'favourite_movies', 'gender', 'hair_colour', 'height', 'name', 'password', 'username', 'weight']

    for field in allFields:
        if( not re.match(r'(password)|(email).*', field) ):
            f = field.title()
            f = re.sub(r'_', ' ', f)
            try:
                value = fields[field]
            except:
                value = ""
            value = re.sub(r'\s*$', '', value)
            valueList = value.split("\n")
            valueString = ""
            valueString += '''<p><textarea class="text" name="%s" rows=%d>%s</textarea></p>''' % (field, len(valueList) + 1, re.sub(r'\t', '', value))
            right += '''        <li class="list-group-item"><b>%s:</b> %s</li> ''' % (f, valueString)

    right += '''<button type="submit" class="btn btn-default" id="saveButton">Save</button></form>'''
    right += '''
        </ul>
    </div>
    '''
    return split( right, panel("Photos", left ) ) + script("elastForms");

def pageControls(page, nextPage, length):
    r = '''
    <p>Page %s of %s</p>''' %(page, length+1)

    if page != 1:
        prevPage = str(int(page) -1)
        r+= '''<form action="love2041.cgi?page=%s" method="post">''' %prevPage
        r+= '''<input type="submit" class="btn btn-default" value="Prev" onclick="prevPage()"/></p></form>'''

    if page < length+1:
        r+= '''<form action="love2041.cgi?page=%s" method="post">''' % nextPage
        r+= '''<input type="submit" class="btn btn-default" value="Next" onclock="nextPage()" /></p></form>'''
    
    r+= '''<form name="logoutForm" action="">'''
    r+= '''<input type="button" class="btn btn-default" value="Logout" onclick="logout()" /></p></form>'''
    return r

def script(s): return '''<script>%s</script>''' % (s)

def backButton(page):
    return '''<div class="row"><form action="love2041.cgi?page=%s" method="post">
    <input type="submit" class="btn btn-default" value="Back" /></p></form></div>''' % page

def alert(text):
    return '''<div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok"></span>    %s
            </div>''' % text

def alertFail(text):
    return '''<div class="alert alert-danger" role="alert">%s</div>''' % text

def register(username, name, degree, email):
    return '''
    <h2>Please enter your details</h2>

    <form enctype="multipart/form-data" action="register.cgi" method="post">
    <p>Username: <input type="text" name="username" value="%s"></p>
    <p>Name: <input type="text" name="name" value="%s"></p>
    <p>Password: <input type="password" name="password"></p>
    <p>Degree: <input type="text" name="degree" value="%s"></p>
    <p>Email: <input type="text" name="email" value="%s"></p>

    <p><h3>Upload profile pic:</h3><input type="file" name="file"></p>

    <p><input type="submit" class="btn btn-default" value="Submit"/></p>
    </form>

    ''' % (username, name, degree, email)

def title():
    return '''<h1>Welcome to LOVE2041</h1>'''

def heading( string ):
    return '''<h2>%s</h2>''' % string

def redirect(url):
    return '''<meta http-equiv="refresh" content="0; url=%s" />''' % url

def split(left, right):
    return '''
    <div class="wrap">
        <div class="row">
                <div class="col-md-6" id="left">%s</div>
                <div class="col-md-6" id="right">%s</div>
        </div>
    </div>
''' % (left, right)

def showPageOf( listEntries, page ):
    a = (page * 10) -10;
    b = a + 10;
    if b > len(listEntries): b = len(listEntries)
    h = ""
    while a < b:
        h += listEntries[a]
        a += 1

    return h

def getFields( username ):
    f = open('students/%s/profile.txt' % (username))
    profileText = ''
    for line in f:
        profileText += line
    f.close()
    profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)
    profileFields = defaultdict(str)

    for x in range(0,len(profileText)):
        if re.match(r'^[a-z_]+:', profileText[x]):
            field = re.sub(r'( *:.*)|(\n*)', '', profileText[x])
            profileFields[field] = profileText[x+1]

    return profileFields

def getPrefs( username ):
    profileFields = defaultdict(str)
    try:
        f = open('students/%s/preferences.txt' % (username))
        profileText = ''
        for line in f:
            profileText += line
        f.close()
        profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)

        for x in range(0,len(profileText)):
            if re.match(r'^[a-z_]+:', profileText[x]):
                field = re.sub(r'( *:.*)|(\n*)', '', profileText[x])
                profileFields[field] = profileText[x+1]
        return profileFields
    except:
        return profileFields

def birthyear( date ):
    bd = re.split("/", date)
    for b in bd:
        if re.search(r'[0-9]{4}', b):
            bi = b
    return b

def inputbox( name, value ):
    v = re.sub(r'\t', '', value)
    if re.match(name, "password"):
        return '''<p><b>%s</b></p><br><p><input type="password" name="%s" value="%s"></p></br>''' % (name.title(), name, v)
    else:
        return '''<p><b>%s</b></p><br><p><input type="text" name="%s" value="%s"></p></br>''' % (name.title(), name, v)

def form():
    return '''<form method="post" action="settings.cgi">'''
def formEnd():
    return '''<button type="submit" class="btn btn-default">Save</button></form>'''

def smallHeading( string ):
    return '''<h2>%s</h2>''' % string









