#!/usr/bin/python

# LOVE2041 COMP2041 14s2 assignment 2
# Aaron Ramshaw - z3461407 - aram150

## Acknowledgements
# All code in the main python files and css are my own work.
# Additional public resources have been used in the boostrap directory,
# except for scripts.js which I wrote


import cgitb, cgi, sys, os, re, os.path, Cookie, html
from collections import defaultdict

def login( username, password ):
    # check login
    if not os.path.isdir('students/%s' % (username)): return 0
    studentsList = os.listdir('students')
    f = open('students/%s/profile.txt' % (username))
    profileText = ''
    for line in f:
        profileText += line
    f.close()
    profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)
    for x in range(0, len(profileText)):
        if re.match(r'^password:', profileText[x]):
            cPassword = profileText[x+1]
    cPassword = re.sub(r'(^\s*)|(\s*$)', '', cPassword)
    if re.match(cPassword, password): return 1
    else: return 0

def findMatches( username, page ):

    # hash based matching algorithm
    # Compares both profile.txt and preferences.txt
    # Values have been altered so that the preferences have a high precedence

    output = []
    studentsList = os.listdir('students')
    outputHTML = ""
    fields = html.getFields(username)
    prefs = html.getPrefs(username)

    k = fields.keys()
    scores = defaultdict(int)
    for s in studentsList:
        matchFields = html.getFields(s)
        for f in k:
            if matchFields.has_key(f) and re.search(r'\W', fields[f]):
                if re.search(re.sub(r'\W', '', matchFields[f]), re.sub(r'\W','', fields[f]) ) or re.search(re.sub(r'\W','', fields[f]), re.sub(r'\W', '', matchFields[f])):
                    if re.search('gender', f): scores[s] += 1
                    else: scores[s] += 2
            if matchFields.has_key(f) and re.search(r'\W', prefs[f]):
                if re.search(re.sub(r'\W', '', matchFields[f]), re.sub(r'\W','', prefs[f]) ) or re.search(re.sub(r'\W','', prefs[f]), re.sub(r'\W', '', matchFields[f])):
                    scores[s] += 10
        if matchFields.has_key("gender"):
            if re.search(matchFields["gender"], fields["gender"]):
                scores[s] = 0
        if matchFields.has_key("birthdate"):
            bd = int( html.birthyear(matchFields["birthdate"]) )
            bd1 = int( html.birthyear(fields["birthdate"]) )
            if bd1/2 + 7 >= bd:
                scores[s] += 2
            if bd > bd1:
                if bd-bd1 < 10:
                    scores[s] += 4
            if bd1 > bd:
                if bd1-bd < 10:
                    scores[s] += 4

        if scores[s] > 0:
            output.append("%d %s" % (scores[s], s))

    # sort based on score, then display the highest matching people
    output.sort(key=scoreCompare , reverse=True)
    listEntries = [];
    for item in output:
        uname = re.sub(r'^[-0-9]+ ', '', item )
        if uname != username:
            listEntries.append( html.profileListEntry(username, uname, fields, html.getFields(uname)) )

    outputHTML += html.showPageOf( listEntries, page )
    return (html.panel("My Matches", outputHTML), len(listEntries)/10)

def scoreCompare( string ):
    return int(re.sub(r' .*', '', string))

def ownProfile(s):

    # Display own profile summary at the top of the home page
    studentsList = os.listdir('students')
    f = open('students/%s/profile.txt' % (s))
    profileText = ''
    for line in f:
        profileText += line
    f.close()
    profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)
    birthdate = ""
    weight = ""
    gender = ""
    height = ""
    email = ""
    name = ""
    degree = ""
    for x in range(0, len(profileText)):
        if re.match(r'^name:', profileText[x]): name = profileText[x+1]
        if re.match(r'^degree:', profileText[x]): degree = profileText[x+1]
        if re.match(r'^birthdate:', profileText[x]): birthdate = profileText[x+1]
        if re.match(r'^email:', profileText[x]): email = profileText[x+1]
        if re.match(r'^weight:', profileText[x]): weight = profileText[x+1]
        if re.match(r'^gender:', profileText[x]): gender = profileText[x+1]
        if re.match(r'^height:', profileText[x]): height = profileText[x+1]

    contents = html.largeProfile(s, name, degree, birthdate, email, gender)
    return html.panel("My Profile", contents)

def homepage(page):
    # print out the home page
    p = ''
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    name = str(cookie['user'].value)
    name = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', name)
    p += ownProfile(name)
    (matches, totalPages) = findMatches(name, page)
    p += matches
    if page + 1 > totalPages:
        nextPage = page
    else: nextPage = page + 1
    p += html.pageControls(page, page+1, totalPages)
    return p

def checkPassword(form):
    p = ""
    if form.has_key('username') and form.has_key('password'):
        if form['username'].value != '' and form['password'].value != '':
            username = form['username'].value
            password = form['password'].value
            if login(username, password) == 1:
                p += html.script('''login("%s")''' % username)
            else:
                p += html.alertFail('''Login fail''')
    return p

cgitb.enable()

form = cgi.FieldStorage()

htmlBody = ""

try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    if str(cookie['loggedIn'].value) == "1":
        user = cookie['user'].value
        try:
            page = int(form['page'].value)
        except:
            page = 1
        htmlBody += html.navbar(user, "matches")
        if form.has_key('newUser'):
            htmlBody += html.alert("Your account has been created. Welcome!")
        htmlBody += homepage(page)      
    else:
        htmlBody += html.loginPage()
        htmlBody += checkPassword(form)
except (Cookie.CookieError, KeyError):
    htmlBody += html.loginPage()
    htmlBody += checkPassword(form)

print html.html(htmlBody)
