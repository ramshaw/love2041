#!/usr/bin/python

import cgitb, cgi, re, sys, os, Cookie, html


cgitb.enable()

form = cgi.FieldStorage()

body = html.title()
fail = 0

try:
    username = form['username'].value
    username = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', username)
    if not re.match(r'[a-zA-Z][a-zA-Z0-9_]+', username):
        body += html.alertfail("Invalid username")
        fail = 1
    if os.path.isdir("students/"+username):
        body += html.alertFail("Username already taken")
        fail = 1
except:
    username = ""
    fail = 1

try:
    name = form['name'].value
    name = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', name)
except:
    name = ""
    fail = 1

try:
    degree = form['degree'].value
    degree = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', degree)
except:
    degree = ""
    fail = 1

try:
    email = form['email'].value
    email = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', email)
    if not re.match(r'.*@.*\..*', email):
        body += html.alertFail("Invalid email")
        fail = 1
except:
    email = ""
    fail = 1

try:
    password = form['password'].value
    password = re.sub(r'(^[\s\t]*)|([\s\t]*$)', '', password)
except:
    password = ""
    fail = 1


try:
    profilePic = form['file']
except:
    fail = 1


if fail == 0:
    try:
        os.mkdir("students/"+username)
        text = "username:\n\t" + username + "\nname:\n\t" + name + "\ndegree:\n\t" + degree + "\npassword:\n\t" + password + "\nemail:\n\t" + email
        f = open('students/'+ username + "/" + "profile.txt", 'w')
        f.write(text)
        f.close()
        f = open('students/'+ username + "/" + "profile.jpg", 'wb')
        f.write(profilePic.file.read())
        f.close()
        body += html.script('''loginNewUser("%s")''' % username)
        body += html.redirect("love2041.cgi?page=1;newUser=1")
    except:
        body += html.alertFail("Failed to make new user")
else:
    body += html.register(username, name, degree, email)

print html.html(body)