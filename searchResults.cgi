#!/usr/bin/python

import cgitb, cgi, re, sys, os, Cookie, html
from collections import defaultdict

cgitb.enable()

form = cgi.FieldStorage()
try:
    query = form['searchQuery'].value
except:
    query = "SexyDude88"

try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    selfUser = cookie['user'].value
except:
    selfUser = "SexyDude88"

selfFields = html.getFields(selfUser)
htmlBody = html.navbar( selfUser, "")

studentsList = os.listdir('students')

matches = ""

for st in studentsList:
    if re.search( query.lower(), st.lower() ) and st != selfUser:
        matches += html.profileListEntry( selfUser, st, selfFields, html.getFields(st) )

print html.html( htmlBody + html.panel(html.heading('''Search results matching "%s"''' % query), matches))