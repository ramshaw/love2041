#!/usr/bin/python

import cgitb, cgi, sys, os, re, os.path, Cookie, html
from collections import defaultdict


form = cgi.FieldStorage()
try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    currUser = cookie['user'].value
except:
    currUser = "SexyDude88"

htmlBody = html.navbar(currUser, "settings")
htmlBody += html.smallHeading("User settings")

# Get profile.txt data
f = open('students/%s/profile.txt' % (currUser))
profileText = ''
for line in f:
    profileText += line
f.close()

profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)
profileFields = defaultdict(str)
existingFields = defaultdict(str)

allFields = ['name', 'email', 'birthdate', 'password']
for x in range(0, len(profileText)):
    if re.match(r'^[a-z_]+:', profileText[x]):
        field = re.sub(r'( *:.*)|(\n*)', '', profileText[x])
        existingFields[field] = profileText[x+1]
        if field in allFields:
        	profileFields[field] = profileText[x+1]

existingKeys = profileFields.keys()
newKeys = form.keys()

modified = 0
for f in allFields:
    if f in newKeys:
        if form[f].value != "":
            new = form[f].value
            newLines = re.split("\n", new)
            newOut = ''
            for n in newLines:
                newOut += "\t" + re.sub(r'\s$', '', n) + "\n"
            if not re.search(profileFields[f], newOut):
                htmlBody += html.alert("Updated " + re.sub(r'_', ' ', f).title())
            elif f not in existingKeys:
                htmlBody += html.alert("Added " + re.sub(r'_', ' ', f).title())
            profileFields[f] = newOut
            modified = 1

# Write updated file
for k in profileFields.keys():
	existingFields[k] = profileFields[k]

if modified == 1:
    keys = existingFields.keys()
    try:
        newProfile = ''
        for k in keys:
            newProfile += k + ":\n" + existingFields[k]
        f = open('students/%s/profile.txt' % (currUser), "w")
        f.write(newProfile)
        f.close()
    except:
        htmlBody += html.alertFail("Could not write to file")

htmlBody += html.form()
for k in profileFields.keys():
	htmlBody += html.inputbox(k, profileFields[k])
htmlBody += html.formEnd()


print html.html(htmlBody)