#!/bin/sh

date=`date +%Y%m%d%H%M%S`
mkdir backups/$date
cp love2041.cgi backups/$date/.
echo "Backed up to "
echo $date
scp * aram150@login.cse.unsw.edu.au:~/public_html/2041
echo "Upload complete"
ssh aram150@login.cse.unsw.edu.au chmod 711 public_html/2041/*.cgi
echo "Set permissions complete"
