#!/usr/bin/python

import cgitb, cgi, re, sys, os, Cookie, html
from collections import defaultdict

cgitb.enable()

# Displays public profile, no editing

form = cgi.FieldStorage()
try:
	user = form['user'].value
except:
	user = "SexyDude88"

try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    currUser = cookie['user'].value
except:
	currUser = "SexyDude88"

htmlBody = html.navbar(currUser, "")

f = open('''students/%s/profile.txt''' % (user))
profileText = ''
for line in f:
	profileText += line
f.close()

profileText = re.split(r'([a-zA-Z_]+:\n)', profileText)

profileFields = defaultdict(str)

# print all available profile fields and values

for x in range(0,len(profileText)):
	if re.match(r'^[a-z_]+:', profileText[x]):
		field = re.sub(r'( *:.*)|(\n*)', '', profileText[x])
		try:
			new = form[field].value
			htmlBody += '''<p> found new value for %s</p>''' % new
			profileFields[field] = new
		except:
			profileFields[field] = profileText[x+1]

photosList = os.listdir('students/'+user)
photos = list()

# Display sidebar of photos, with nice image viewer.

for i in photosList:
	if re.match(r'photo[0-9][0-9].[(jpg)(png)]', i):
		photos.append(i)

htmlBody += html.fullProfile(profileFields, photos)

try:
    cookie = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
    page = str(cookie['currPage'].value)
except:
    page = "1"

htmlBody += html.backButton(page)

print html.html(htmlBody)